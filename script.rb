class Integer
    # Compute self ^ e mod m
    def mod_exp e, m
        result = 1
        b = self
        while e > 0
            result = (result * b) % m if e[0] == 1
            e = e >> 1
            b = (b * b) % m
        end
        return result
    end

    #counting bits.
    def bits_set
        to_s(2).count('1')
    end
end


class DH
    attr_reader :p, :g, :q, :x, :e

    # p is the open prime number,
    # g the generator,
    # q order of the subgroup (limits for secret numbers generator)
    def initialize p, g, q
        @p = p
        @g = g
        @q = q
    end

    # generate the [secret] random value and the public key
    def generate tries=16
        tries.times do
            @x = rand(@q) #a/b/c,...
            @e = self.g.mod_exp(@x, self.p) #public key
            return @e if self.valid?
        end
        raise ArgumentError, "can't generate valid e"
    end

    # validate a public key
    def valid? _e = self.e
        _e and _e.between?(2, self.p-2) and _e.bits_set > 1
    end

    # compute the shared secret key, given the public key
    def secret f
        f.mod_exp(self.x, self.p)
    end
end

p = 7
g = 3

puts "*"*20
puts "2 person exchange"
puts "*"*20

puts "Seleted prime p=#{p}"
puts "Seleted generator g=#{g}"

limit_a = 19
limit_b = 17

alice = DH.new(p, g, limit_a)
bob   = DH.new(p, g, limit_b)

alice.generate
bob.generate

a = alice.x
b = bob.x

puts "Generated a for Alice. a=#{a}"
puts "Generated b for Bob. b=#{b}"

puts "Alice gets Bob public key\nBob gets Alice public key"

puts "Generated public key for Alice.=#{alice.e}"
puts "Generated public key for Bob. =#{bob.e}"


alice_s = alice.secret(bob.e)
bob_s   = bob.secret(alice.e)

puts "Shared secret key, generated by Alice = #{alice_s}"
puts "Shared secret key, generated by Bob = #{bob_s}"

puts "*"*20
puts "3 person exchange"
puts "*"*20

puts "Seleted prime p=#{p}"
puts "Seleted generator g=#{g}"

limit_a = 137
limit_b = 151
limit_c = 171

alice = DH.new(p, g, limit_a)
bob   = DH.new(p, g, limit_b)
carol = DH.new(p, g, limit_c)

alice.generate
bob.generate
carol.generate

alice_pb = alice.e
bob_pb = bob.e
carol_pb = carol.e

puts "Generated secret a for Alice. a=#{alice.e}"
puts "Generated secret b for Bob. b=#{bob.e}"
puts "Generated secret c for Carol. c=#{carol.e}"

puts "Alice Bob sends to Alice"
bob_alice = alice_pb.mod_exp(bob_pb, p)

puts "Bob sends to Carol"
carol_bob = carol_pb.mod_exp(bob_pb, p)

puts "Alice generates secret from (Carol+Bob)"
alice_s = alice_carol_bob = carol_bob.mod_exp(alice_pb, p)

puts "Alice sends to Carol"
alice_carol = carol_pb.mod_exp(alice_pb, p)

puts "Bob generates secret from (Alice+Carol)"
bob_s = alice_carol.mod_exp(bob_pb, p)

puts "Optional secret for Carol"
carol_s = carol_bob.mod_exp(carol_pb, p)

puts "Shared secret key, generated by Alice = #{alice_s}"
puts "Shared secret key, generated by Bob = #{bob_s}"
puts "Shared secret key, for carol #{carol_s}"
